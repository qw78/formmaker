## Simple form making process ##
### Easily build professional online form making and web surveys ###
Looking for forms? Choose from over 1000+ pre-built web form templates that you can customize for registrations, reservations, secure orders, customer surveys and payment collection 

**Our features:**  

* Form building
* Optimization
* A/B testing
* Multiple language
* Custom templates
* Theme creation   

### Well defined themes and templates complete your form need fully ###
Our Drag & Drop [form maker](https://formtitan.com), combined with over 40 question types, enables you to easily design almost any type of web form or survey  

Happy form maker!